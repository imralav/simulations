package dissimlab.resourceManagement.warehouse.exceptions;

public class UnregisteredResourceRequested extends Exception {
    public UnregisteredResourceRequested(String msg) {
        super(msg);
    }
}
