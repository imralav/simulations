package dissimlab.resourceManagement.warehouse.events;

import dissimlab.resourceManagement.warehouse.BasicResourceObject;
import dissimlab.resourceManagement.warehouse.BasicWarehouse;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;

public class ReturnResourceEvent extends BasicSimEvent<BasicWarehouse, Object> {

    private BasicResourceObject resource;
    private BasicSimObj requestingObject;

    public ReturnResourceEvent(BasicWarehouse parentWarehouse, BasicSimObj requestingObject, BasicResourceObject resource,
            double resourceBorrowedForTime) throws SimControlException {
        super(parentWarehouse, resourceBorrowedForTime); 
        this.requestingObject = requestingObject;
        this.resource = resource;
    }

    @Override
    public Object getEventParams() {
        return null;
    }

    @Override
    protected void stateChange() throws SimControlException {
        requestingObject.removeResource(resource);
        getSimObj().returnResource(resource);
    }

    @Override
    protected void onTermination() throws SimControlException {
        
    }

    @Override
    protected void onInterruption() throws SimControlException {
        
    }

}
