package dissimlab.resourceManagement.warehouse.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dissimlab.resourceManagement.warehouse.BasicWarehouse;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;

public class CustomerImpatienceEvent extends BasicSimEvent<BasicWarehouse, Object> {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private BasicSimObj requestingObject;
    private Class<?> resourceClass;

    public CustomerImpatienceEvent(BasicWarehouse basicWarehouseObj, BasicSimObj requestingObject,
            Class<?> resourceRequest, double requestorsWaitingTime) throws SimControlException {
        super(basicWarehouseObj, requestorsWaitingTime);
        this.requestingObject = requestingObject;
        this.resourceClass = resourceRequest;
    }

    @Override
    public Object getEventParams() {
        return null;
    }

    @Override
    protected void stateChange() throws SimControlException {
        logger.info("{} {} is fed up with waiting for {}, removing it from waiting queue", simTime(), requestingObject, resourceClass);
        getSimObj().removeCustomerFromQueue(resourceClass, requestingObject);
    }

    @Override
    protected void onTermination() throws SimControlException {

    }

    @Override
    protected void onInterruption() throws SimControlException {

    }

}
