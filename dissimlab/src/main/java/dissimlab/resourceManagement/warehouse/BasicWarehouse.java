package dissimlab.resourceManagement.warehouse;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.resourceManagement.warehouse.events.CustomerImpatienceEvent;
import dissimlab.resourceManagement.warehouse.events.ReturnResourceEvent;
import dissimlab.resourceManagement.warehouse.exceptions.UnregisteredResourceRequested;
import dissimlab.resourceManagement.warehouse.renewal.NopRenewalStrategy;
import dissimlab.resourceManagement.warehouse.renewal.ResourceRenewalStrategy;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;
import pl.edu.wat.simulations.queues.FifoQueue;
import pl.edu.wat.simulations.queues.Queue;

public class BasicWarehouse extends BasicSimObj {
    private static final Logger logger = LoggerFactory.getLogger(BasicWarehouse.class);

    private Map<Class<? extends BasicResourceObject>, Queue<BasicResourceObject>> resourceMap;
    private Map<Class<? extends BasicResourceObject>, Queue<BasicSimObj>> waitingCustomersMap;
    private Map<Class<? extends BasicResourceObject>, Integer> initialResourcesAmounts;
    /**
     * Allows to quickly lookup whether a {@link BasicResourceObject} is returnable or not
     */
    private Map<Class<? extends BasicResourceObject>, Boolean> resourceReturnabilityMap;
    private ResourceRenewalStrategy renewalStrategy;

    public BasicWarehouse() {
        this(null);
    }

    public BasicWarehouse(ResourceRenewalStrategy renewalStrategy) {
        resourceMap = new HashMap<>();
        waitingCustomersMap = new HashMap<>();
        initialResourcesAmounts = new HashMap<>();
        resourceReturnabilityMap = new HashMap<>();
        if (renewalStrategy == null) {
            renewalStrategy = new NopRenewalStrategy();
        }
        renewalStrategy.setWarehouse(this);
        this.renewalStrategy = renewalStrategy;
    }

    /**
     * Registers new resource in the warehouse.
     * 
     * @param resourceClass
     *            class of the registered resource, needs to extend {@link BasicResourceObject}
     * @param isReturnable
     *            specifies whether a resource will be returned from the requestor after borrowed time in
     *            {@link BasicWarehouse#orderRequest(BasicSimObj, Class, int, int)}. If not, the warehouse might run out
     *            of the resource depending on specified ResourceRenewalStrategy
     * @param initialAmount
     *            initial amount of resources in the warehouse
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    public <T extends BasicResourceObject> void registerResource(Class<T> resourceClass, boolean isReturnable,
            int initialAmount) throws InstantiationException, IllegalAccessException, IllegalArgumentException,
                    InvocationTargetException, NoSuchMethodException, SecurityException {
        logger.info("{} Registering {} (renewable: {}, initial amount: {})",
                simTime(),
                resourceClass.getSimpleName(),
                isReturnable,
                initialAmount);
        if (resourceMap.containsKey(resourceClass)) {
            logger.error("{} {} not registered - it has already been registered before",
                    simTime(),
                    resourceClass.getSimpleName());
            throw new UnsupportedOperationException(
                    "This resource has already been registered (" + resourceClass + ")");
        }
        Queue<BasicResourceObject> queue = new FifoQueue<>();
        addResourcesToQueue(resourceClass, isReturnable, initialAmount, queue);
        resourceMap.put(resourceClass, queue);
        waitingCustomersMap.put(resourceClass, new FifoQueue<>());
        initialResourcesAmounts.put(resourceClass, initialAmount);
        resourceReturnabilityMap.put(resourceClass, isReturnable);
    }

    /**
     * Orders a resource from a warehouse. If it is available it is returned instantly. Otherwise, null is returned and
     * customer awaits for the resource (warehouse notifies customers)
     * 
     * @param requestingObject
     *            customer ordering a resource
     * @param resourceRequest
     *            class of a resource
     * @param requestorsWaitingTime
     *            how much will the customer wait for the resource
     * @param resourceBorrowedForTime
     *            for how long will the customer borrow the resource
     * @return
     * @throws UnregisteredResourceRequested
     */
    @SuppressWarnings("unchecked")
    public <T extends BasicResourceObject> T orderRequest(BasicSimObj requestingObject, Class<T> resourceRequest,
            int requestorsWaitingTime, int resourceBorrowedForTime) throws UnregisteredResourceRequested {
        logger.info("{} Resource of class {} ordered by {}. Await time: {}",
                simTime(),
                resourceRequest.getSimpleName(),
                requestingObject,
                requestorsWaitingTime);
        if (!resourceMap.containsKey(resourceRequest)) {
            throw new UnregisteredResourceRequested(
                    "The requested resource (" + resourceRequest.getSimpleName() + ") has not been registered");
        }
        optionallyRenewOrderedResource(resourceRequest);
        Queue<? extends BasicResourceObject> resourceQueue = resourceMap.get(resourceRequest);
        if (resourceQueue.size() > 0) {
            BasicResourceObject resource = resourceMap.get(resourceRequest).popElement();
            requestingObject.addResource(resource);
            if (resource.isReturnable()) {
                fireReturnResourceEvent(requestingObject, resourceBorrowedForTime, resource);
            }
            return (T) resource;
        } else {
            waitingCustomersMap.get(resourceRequest).addElement(requestingObject);
            fireCustomerImpatienceEvent(requestingObject, resourceRequest, requestorsWaitingTime);
            return null;
        }
    }

    /**
     * Returns borrowed resource to the collection and notifies customers waiting for this resource
     * 
     * @param returnedResource
     */
    public void returnResource(BasicResourceObject returnedResource) {
        logger.info("{} Resource {} returned to {}", simTime(), returnedResource, this);
        resourceMap.get(returnedResource.getClass()).addElement(returnedResource);
        Queue<BasicSimObj> queue = waitingCustomersMap.get(returnedResource.getClass());
        if (queue.size() > 0) {
            BasicSimObj customer = queue.popElement();
            logger.info("{} Notifying awaiting customer {}", simTime(), customer);
            customer.resourceReturned(returnedResource.getClass(), this);
        }
    }

    @Override
    public void reflect(IPublisher publisher, INotificationEvent event) {

    }

    @Override
    public boolean filter(IPublisher publisher, INotificationEvent event) {
        return false;
    }

    @Override
    public void resourceReturned(Class<? extends BasicResourceObject> returnedResourceClass, BasicWarehouse warehouse) {
    }

    public void removeCustomerFromQueue(Class<?> resourceClass, BasicSimObj requestingObject) {
        Queue<BasicSimObj> queue = waitingCustomersMap.get(resourceClass);
        queue.popElement(requestingObject);
        logger.info("{} customer {} removed from waiting queue for resource {}",
                simTime(),
                requestingObject,
                resourceClass.getSimpleName());
    }

    public void removeCustomerFromAllQueues(BasicSimObj requestingObject) {
        waitingCustomersMap.values().forEach(queue -> {
            queue.popElement(requestingObject);
        });
    }

    public Stream<Class<? extends BasicResourceObject>> getResourceClasses() {
        return resourceMap.keySet().stream();
    }

    public int getActualResourceAmount(Class<? extends BasicResourceObject> resourceRequest) {
        return this.resourceMap.get(resourceRequest).size();
    }

    public int getMaxResourceAmount(Class<? extends BasicResourceObject> resourceRequest) {
        return this.initialResourcesAmounts.get(resourceRequest).intValue();
    }

    public Map<Class<? extends BasicResourceObject>, Queue<BasicResourceObject>> getResourceMap() {
        return resourceMap;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [resourceMap=" + resourceMap + ", waitingCustomersMap="
                + waitingCustomersMap + "]";
    }

    private <T extends BasicResourceObject> void addResourcesToQueue(Class<T> resourceClass, boolean isRenewable,
            int resourcesToAdd, Queue<BasicResourceObject> queue) throws InstantiationException, IllegalAccessException,
                    InvocationTargetException, NoSuchMethodException {
        for (int i = 0; i < resourcesToAdd; i++) {
            queue.addElement(resourceClass.getConstructor(int.class, boolean.class).newInstance(
                    ThreadLocalRandom.current().nextInt(), isRenewable));
        }
    }

    private <T extends BasicResourceObject> void fireCustomerImpatienceEvent(BasicSimObj requestingObject,
            Class<T> resourceRequest, int requestorsWaitingTime) {
        try {
            new CustomerImpatienceEvent(this, requestingObject, resourceRequest, requestorsWaitingTime);
        } catch (SimControlException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void fireReturnResourceEvent(BasicSimObj requestingObject, int resourceBorrowedForTime,
            BasicResourceObject resource) {
        try {
            new ReturnResourceEvent(this, requestingObject, resource, resourceBorrowedForTime);
        } catch (SimControlException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private <T extends BasicResourceObject> boolean shouldRenewResource(Class<T> resourceRequest) {
        return !resourceReturnabilityMap.get(resourceRequest)
                && renewalStrategy.shouldRenewResource(simTime(), resourceRequest);
    }

    private <T extends BasicResourceObject> void optionallyRenewOrderedResource(Class<T> resourceRequest) {
        if (shouldRenewResource(resourceRequest)) {
            logger.info("{}: {} decided that the resource of class {} should be renewed",
                    simTime(),
                    renewalStrategy.getClass().getSimpleName(),
                    resourceRequest.getSimpleName());
            int actualAmount = getActualResourceAmount(resourceRequest);
            int maxAmount = getMaxResourceAmount(resourceRequest);
            logger.info("{}: Adding {} of {}", simTime(), maxAmount - actualAmount, resourceRequest.getSimpleName());
            try {
                addResourcesToQueue(resourceRequest, false, maxAmount - actualAmount, resourceMap.get(resourceRequest));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                    | NoSuchMethodException e) {
                logger.error("Couldn't add resources of class {} to queue, because: ", resourceRequest, e);
            }
        }
    }
}
