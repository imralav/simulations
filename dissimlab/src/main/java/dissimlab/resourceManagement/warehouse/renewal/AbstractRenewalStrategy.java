package dissimlab.resourceManagement.warehouse.renewal;

import dissimlab.resourceManagement.warehouse.BasicWarehouse;

public abstract class AbstractRenewalStrategy implements ResourceRenewalStrategy {
    protected BasicWarehouse warehouse;

    @Override
    public void setWarehouse(BasicWarehouse warehouse) {
        this.warehouse = warehouse;
    }
}
