package dissimlab.resourceManagement.warehouse.renewal;

import java.util.function.BiFunction;

import dissimlab.resourceManagement.warehouse.BasicResourceObject;

public class ResourceAmountRenewalStrategy extends AbstractRenewalStrategy {

    private BiFunction<Integer, Integer, Boolean> shouldRenewPredicate;

    public ResourceAmountRenewalStrategy(BiFunction<Integer, Integer, Boolean> shouldRenewPredicate) {
        this.shouldRenewPredicate = shouldRenewPredicate;
    }

    @Override
    public boolean shouldRenewResource(double simTime, Class<? extends BasicResourceObject> resourceClass) {
        return shouldRenewPredicate.apply(warehouse.getActualResourceAmount(resourceClass),
                warehouse.getMaxResourceAmount(resourceClass));
    }

}
