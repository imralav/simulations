package dissimlab.resourceManagement.warehouse.renewal;

import dissimlab.resourceManagement.warehouse.BasicResourceObject;
import dissimlab.resourceManagement.warehouse.BasicWarehouse;

public interface ResourceRenewalStrategy {
    /**
     * Decides whether a resource should be restored depending on current simulation time and/or state of the warehouse
     * 
     * @param simTime
     * @param warehouse
     * @return
     */
    public boolean shouldRenewResource(double simTime, Class<? extends BasicResourceObject> resourceClass);

    public void setWarehouse(BasicWarehouse warehouse);
}
