package dissimlab.resourceManagement.warehouse.renewal;

import dissimlab.resourceManagement.warehouse.BasicResourceObject;

/**
 * Defaults to always returning false when warehouse asks whether the resource should be returned and renew method does
 * nothing
 * 
 * @author karczewt
 *
 */
public class NopRenewalStrategy extends AbstractRenewalStrategy {
    @Override
    public boolean shouldRenewResource(double simTime, Class<? extends BasicResourceObject> resourceClass) {
        return false;
    }

}
