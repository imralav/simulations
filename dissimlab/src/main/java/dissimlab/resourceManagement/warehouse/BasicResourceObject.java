package dissimlab.resourceManagement.warehouse;

public abstract class BasicResourceObject {
    private int id;
    private boolean isReturnable;

    public BasicResourceObject(int id, boolean isReturnable) {
        this.id = id;
        this.isReturnable = isReturnable;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "BasicResourceObject [id=" + id + ", isReturnable=" + isReturnable + "]";
    }

    public boolean isReturnable() {
        return isReturnable;
    }
}
