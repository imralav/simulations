package dissimlab.simcore;

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.resourceManagement.warehouse.BasicResourceObject;
import dissimlab.resourceManagement.warehouse.BasicWarehouse;



/**
 * Description...
 * 
 * @author Dariusz Pierzchala
 *
 */
public class SimContextEntity extends BasicSimObj {

	public SimContextEntity(SimContext context) {
		super(context);
	}

	@Override
	public void reflect(IPublisher publisher, INotificationEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean filter(IPublisher publisher, INotificationEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

    @Override
    public void resourceReturned(Class<? extends BasicResourceObject> returnedResourceClass,
            BasicWarehouse warehouse) {
        // TODO Auto-generated method stub
        
    }

}
