package dissimlab.resourceManagement.warehouse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.Ignore;
import org.junit.Test;

import dissimlab.resourceManagement.warehouse.exceptions.UnregisteredResourceRequested;

class TestResource extends BasicResourceObject {
	public TestResource(int id, boolean isRenewable) {
		super(id, isRenewable);
	}
}

public class BasicWarehouseTest {
	BasicWarehouse instance = new BasicWarehouse();
	@Test
	public void shouldRegisterCorrectResourceAndAmount() throws Exception {
		//given
		//when
		instance.registerResource(TestResource.class, true, 10);
		//then
		assertThat(instance.getResourceMap()).containsOnlyKeys(TestResource.class);
		assertThat(instance.getResourceMap().get(TestResource.class).size()).isEqualTo(10);
	}
	
	@Test
    public void shouldBreakForDuplicateRegistration() throws Exception {
        //given
        //when
        //then
	    instance.registerResource(TestResource.class, false, 10);
	    assertThatThrownBy(() -> {
	        instance.registerResource(TestResource.class, false, 10);
	    }).isInstanceOf(UnsupportedOperationException.class).hasMessageContaining("This resource has already been registered").hasMessageContaining(TestResource.class.toString());
    }
	
	@Test
    public void shouldThrowExceptionWhenResourceNotRegistered() throws Exception {
        //given
        //when
        //then
	    assertThatThrownBy(() -> {
	        instance.orderRequest(null, TestResource.class, 0, 0); 
	    }).isInstanceOf(UnregisteredResourceRequested.class)
	    .hasMessageContaining(TestResource.class.getSimpleName());
    }
	
	@Test
	@Ignore
    public void shouldReturnResourceWhenItsAvailable() throws Exception {
        //given
	    instance.registerResource(TestResource.class, true, 1);
        //when
	    TestResource result = instance.orderRequest(null, TestResource.class, 10, 0);
        //then
	    assertThat(result).isNotNull().isInstanceOf(TestResource.class);
	    assertThat(result.getId()).isEqualTo(1);
    }
}
