package smo.model;
/**
 * @author Dariusz Pierzchala
 * 
 * Description: Description: Klasa gniazda obsługi obiektów - zgłoszeń 
 */

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.monitors.MonitoredVar;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimEventSemaphore;
import pl.edu.wat.simulations.queues.PriorityQueue;
import pl.edu.wat.simulations.queues.Queue;
import smo.events.BeginServiceEvent;
import smo.events.EndServiceEvent;

public class Smo extends BasicSimObj {
	private Queue<SmoAnnouncement> queue;
	private SmoBis smo2;
	SimEventSemaphore semafor;
	private boolean availableForService = true;
	private BeginServiceEvent beginServiceEvent;
	public EndServiceEvent endServiceEvent;
	public MonitoredVar monitoredServiceTime;
	public MonitoredVar monitoredWaitingTime;
	public MonitoredVar monitoredQueueSize;
	public MonitoredVar monitoredAmountOfLostAnnouncements;

	/**
	 * Creates a new instance of Smo
	 * 
	 * @throws SimControlException
	 */
	public Smo(SmoBis smo, SimEventSemaphore semafor) throws SimControlException {
		// Utworzenie wewnętrznej listy w kolejce
		queue = new PriorityQueue<>();
		// Nastepne SMO
		smo2 = smo;
		// Deklaracja zmiennych monitorowanych
		monitoredServiceTime = new MonitoredVar();
		monitoredWaitingTime = new MonitoredVar();
		monitoredQueueSize = new MonitoredVar();
		monitoredAmountOfLostAnnouncements = new MonitoredVar();
		this.semafor = semafor;
	}

	// Wstawienie zgłoszenia do kolejki
	public int addEntry(SmoAnnouncement zgl) {
		queue.addElement(zgl);
		monitoredQueueSize.setValue(queue.size());
		return queue.size();
	}

	// Pobranie zgłoszenia z kolejki
	public SmoAnnouncement removeEntry() {
		SmoAnnouncement zgl = (SmoAnnouncement) queue.popElement();
		monitoredQueueSize.setValue(queue.size());
		return zgl;
	}

	// Pobranie zgłoszenia z kolejki
	public boolean removeEntry(SmoAnnouncement zgl) {
		Boolean b = queue.popElement(zgl);
		monitoredQueueSize.setValue(queue.size());
		return b;
	}

	public int getAmountOfEntries() {
		return queue.size();
	}

	public boolean isAvailableForHandling() {
		return availableForService;
	}

	public void setAvailableForHandling(boolean isAvailableForHandling) {
		this.availableForService = isAvailableForHandling;
	}

	public SmoBis getSmo2() {
		return smo2;
	}

	public void setSmo2(SmoBis smo2) {
		this.smo2 = smo2;
	}

	public SimEventSemaphore getSemafor() {
		return semafor;
	}

	public void setSemafor(SimEventSemaphore semafor) {
		this.semafor = semafor;
	}

	@Override
	public void reflect(IPublisher publisher, INotificationEvent event) {

	}

	@Override
	public boolean filter(IPublisher publisher, INotificationEvent event) {
		return false;
	}

	public BeginServiceEvent getBeginServiceEvent() {
		return beginServiceEvent;
	}

	public void setBeginServiceEvent(BeginServiceEvent beginServiceEvent) {
		this.beginServiceEvent = beginServiceEvent;
	}
}