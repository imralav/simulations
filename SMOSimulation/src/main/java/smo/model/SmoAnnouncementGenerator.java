package smo.model;

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.monitors.MonitoredVar;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;
import smo.events.AnnounceEvent;


public class SmoAnnouncementGenerator extends BasicSimObj {
    public AnnounceEvent announce;
    private MonitoredVar timeBetweenAnnouncementsMonitor;
    private Smo smo;

	public SmoAnnouncementGenerator(Smo smo) throws SimControlException {
        // Powołanie instancji pierwszego zdarzenia
    	announce = new AnnounceEvent(this, 0.0);
        // Deklaracja zmiennych monitorowanych
        timeBetweenAnnouncementsMonitor = new MonitoredVar();
        // SMO dla zgłoszeń
        this.smo = smo;
	}

	@Override
	public void reflect(IPublisher publisher, INotificationEvent event) {
		
	}

	@Override
	public boolean filter(IPublisher publisher, INotificationEvent event) {
		return false;
	}

    public Smo getSmo() {
		return smo;
	}

	public void setSmo(Smo smo) {
		this.smo = smo;
	}

	public MonitoredVar getTimeBetweenAnnouncementsMonitor() {
		return timeBetweenAnnouncementsMonitor;
	}
}
