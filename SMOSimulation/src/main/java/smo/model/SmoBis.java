package smo.model;
/**
 * @author Dariusz Pierzchala
 * 
 * Description: Klasa gniazda obsługi obiektów - zgłoszeń 
 */

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.monitors.MonitoredVar;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimEventSemaphore;
import pl.edu.wat.simulations.queues.PriorityQueue;
import pl.edu.wat.simulations.queues.Queue;
import smo.events.BeginServiceBisEvent;
import smo.events.EndServiceBisEvent;

public class SmoBis extends BasicSimObj {
	private Queue<SmoAnnouncement> queue;
	private boolean availableForService = true;
	private int maximumQueueSize;
	//
	SimEventSemaphore semaphore;
	//
	public BeginServiceBisEvent beginServiceEvent;
	public EndServiceBisEvent endServiceEvent;
	// public OdblokujGniazdo odblokuj;
	//
	public MonitoredVar monitoredServiceTime;
	public MonitoredVar monitoredWaitingTime;
	public MonitoredVar monitoredQueueSize;

	/**
	 * Creates a new instance of SmoBis
	 * 
	 * @throws SimControlException
	 */
	public SmoBis(int maximumQueueSize, SimEventSemaphore semaphore) throws SimControlException {
		queue = new PriorityQueue<>(maximumQueueSize, true);
		this.maximumQueueSize = maximumQueueSize;
		this.semaphore = semaphore;
		//
		// Deklaracja zmiennych monitorowanych
		monitoredServiceTime = new MonitoredVar();
		monitoredWaitingTime = new MonitoredVar();
		monitoredQueueSize = new MonitoredVar();
	}

	// Wstawienie zgłoszenia do kolejki
	public boolean addEntry(SmoAnnouncement announcement) {
		if (getAmountOfEntries() < maximumQueueSize) {
			queue.addElement(announcement);
			monitoredQueueSize.setValue(queue.size());
			return true;
		}
		return false;
	}

	// Pobranie zgłoszenia z kolejki
	public SmoAnnouncement removeEntry() {
		SmoAnnouncement announcement = queue.popElement();
		monitoredQueueSize.setValue(queue.size());
		return announcement;
	}

	// Pobranie zgłoszenia z kolejki
	public boolean removeEntry(SmoAnnouncement announcement) {
		Boolean b = queue.popElement(announcement);
		monitoredQueueSize.setValue(queue.size());
		return b;
	}

	public int getAmountOfEntries() {
		return queue.size();
	}

	public boolean isAvailableForServie() {
		return availableForService;
	}

	public void setAvailableForService(boolean wolne) {
		this.availableForService = wolne;
	}

	public SimEventSemaphore getSemafor() {
		return semaphore;
	}

	public void setSemafor(SimEventSemaphore semafor) {
		this.semaphore = semafor;
	}

	public int getMaximumQueueSize() {
		return maximumQueueSize;
	}

	public void setMaximumQueueSize(int maximumQueueSize) {
		this.maximumQueueSize = maximumQueueSize;
	}

	@Override
	public void reflect(IPublisher publisher, INotificationEvent event) {

	}

	@Override
	public boolean filter(IPublisher publisher, INotificationEvent event) {
		return false;
	}
}