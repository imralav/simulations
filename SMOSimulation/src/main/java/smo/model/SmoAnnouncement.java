package smo.model;

import smo.events.EndImpatienceEvent;
import smo.model.Smo;
import smo.events.BeginImpatienceEvent;
import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;

/**
 * Description: Klasa zgloszenia obsługiwanego w gnieździe obsługi.
 * 
 * @author Dariusz Pierzchala
 */

public class SmoAnnouncement extends BasicSimObj implements Comparable<SmoAnnouncement> {
	private int priority = 0;

	double czasOdniesienia;
	static int nr = 0;
	int tenNr;
	public Smo parentSmo;
	public EndImpatienceEvent endImpatienceEvent;

	public int getTenNr() {
		return tenNr;
	}

	public void setTenNr() {
		this.tenNr = nr++;
	}

	public SmoAnnouncement(double Czas, Smo parentSmo) throws SimControlException {
		czasOdniesienia = Czas;
		setTenNr();
		this.parentSmo = parentSmo;
		new BeginImpatienceEvent(this);
	}

	public void setCzasOdniesienia(double t) {
		czasOdniesienia = t;
	}

	public double getCzasOdniesienia() {
		return czasOdniesienia;
	}

	@Override
	public void reflect(IPublisher publisher, INotificationEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean filter(IPublisher publisher, INotificationEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int compareTo(SmoAnnouncement comparedEntry) {
		return priority - comparedEntry.getPriority();
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "SmoAnnouncement [priority=" + priority + ", tenNr=" + tenNr + "]";
	}
}