package smo.events;

import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimParameters.SimDateField;
import smo.model.SmoAnnouncement;
import smo.model.SmoAnnouncementGenerator;

/**
 * Description: Zdarzenie generatora zgloszen. Tworzy obiekt - zgloszenie co
 * losowy czas.
 * 
 * @author Dariusz Pierzchala
 * 
 */
public class AnnounceEvent extends BasicSimEvent<SmoAnnouncementGenerator, Object> {
	private SimGenerator generator;
	private SmoAnnouncementGenerator parent;

	public AnnounceEvent(SmoAnnouncementGenerator parent, double delay) throws SimControlException {
		super(parent, delay);
		generator = new SimGenerator();
	}

	public AnnounceEvent(SmoAnnouncementGenerator parent) throws SimControlException {
		super(parent);
		generator = new SimGenerator();
	}

	@Override
	protected void onInterruption() throws SimControlException {

	}

	@Override
	protected void onTermination() throws SimControlException {

	}

	@Override
	protected void stateChange() throws SimControlException {
		parent = getSimObj();
		SmoAnnouncement zgl = prepareNewSmoAnnouncement();
		parent.getSmo().addEntry(zgl);
		System.out.println(simDate(SimDateField.HOUR24) + " - " + simDate(SimDateField.MINUTE) + " - "
				+ simDate(SimDateField.SECOND) + " - " + simDate(SimDateField.MILLISECOND) + ": Added new "
				+ zgl);
		// Aktywuj obsługę, jeżeli kolejka była pusta (gniazdo "spało")
		if (parent.getSmo().getAmountOfEntries() == 1 && parent.getSmo().isAvailableForHandling()) {
			parent.getSmo().setBeginServiceEvent(new BeginServiceEvent(parent.getSmo()));
		}
		// Wygeneruj czas do kolejnego zgłoszenia
		double odstep = generator.normal(5.0, 1.0);
		parent.getTimeBetweenAnnouncementsMonitor().setValue(odstep);
		setRepetitionPeriod(odstep);
		// alternatywnie: parent.zglaszaj = new Zglaszaj(parent, odstep);
	}

	private SmoAnnouncement prepareNewSmoAnnouncement() throws SimControlException {
		SmoAnnouncement zgl = new SmoAnnouncement(simTime(), parent.getSmo());
		zgl.setPriority(generator.nextInt(10));
		return zgl;
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}
}