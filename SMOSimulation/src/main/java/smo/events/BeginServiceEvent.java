package smo.events;
/**
 * @author Dariusz Pierzchala
 * 
 * Description: Zdarzenie początkowe aktywności gniazda obsługi. Rozpoczyna obsługę przez losowy czas obiektów - zgłoszeń.
 */

import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import smo.model.Smo;
import smo.model.SmoAnnouncement;

public class BeginServiceEvent extends BasicSimEvent<Smo, SmoAnnouncement> {
	private Smo smoParent;
	private SimGenerator generator;

	public BeginServiceEvent(Smo parentSmo, double delay) throws SimControlException {
		super(parentSmo, delay);
		generator = new SimGenerator();
		this.smoParent = parentSmo;
	}

	public BeginServiceEvent(Smo parent) throws SimControlException {
		super(parent);
		generator = new SimGenerator();
		this.smoParent = parent;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void stateChange() throws SimControlException {
		if (smoParent.getAmountOfEntries() > 0) {
			// Zablokuj gniazdo
			smoParent.setAvailableForHandling(false);
			// Pobierz zgłoszenie
			SmoAnnouncement zgl = smoParent.removeEntry();
			// Przerwanie niecierpliwosci
			zgl.endImpatienceEvent.interrupt();
			// Wygeneruj czas obsługi
			double handlingTime = generator.normal(9.0, 1.0);
			// Zapamiętaj dane monitorowane
			smoParent.monitoredWaitingTime.setValue(simTime() - zgl.getCzasOdniesienia(), simTime());
			zgl.setCzasOdniesienia(simTime());
			System.out.println(simTime() + ": Began processing " + zgl);
			// Zaplanuj koniec obsługi
			smoParent.endServiceEvent = new EndServiceEvent(smoParent, handlingTime, zgl);
		}

	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}
}