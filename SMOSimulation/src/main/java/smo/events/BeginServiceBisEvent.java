package smo.events;
/**
 * @author Dariusz Pierzchala
 * 
 * Description: Zdarzenie początkowe aktywności gniazda obsługi. Rozpoczyna obsługę przez losowy czas obiektów - zgłoszeń.
 */

import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import smo.model.SmoAnnouncement;
import smo.model.SmoBis;

public class BeginServiceBisEvent extends BasicSimEvent<SmoBis, SmoAnnouncement> {
	private SmoBis smoParent;
	private SimGenerator generator;

	public BeginServiceBisEvent(SmoBis parent, double delay) throws SimControlException {
		super(parent, delay);
		generator = new SimGenerator();
		this.smoParent = parent;
	}

	public BeginServiceBisEvent(SmoBis parent) throws SimControlException {
		super(parent);
		generator = new SimGenerator();
		this.smoParent = parent;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void stateChange() throws SimControlException {
		if (smoParent.getAmountOfEntries() > 0) {
			// Zablokuj gniazdo
			smoParent.setAvailableForService(false);
			// Pobierz zgłoszenie
			SmoAnnouncement zgl = smoParent.removeEntry();
			// Otwarcie semafora blokującego gniazdo 1-sze
			if (smoParent.getAmountOfEntries() == smoParent.getMaximumQueueSize() - 1) {
				try {
					System.out.println(simTime() + ": SMO2- otwarcie semafora - zwolnienie: "
							+ smoParent.getSemafor().readFirstBlocked().toString());
				} catch (Exception e) {
				}
				smoParent.getSemafor().open();
			}
			// Wygeneruj czas obsługi
			double czasObslugi = generator.normal(10.0, 1.0);
			// Zapamiętaj dane monitorowane
			smoParent.monitoredServiceTime.setValue(czasObslugi);
			smoParent.monitoredWaitingTime.setValue(simTime() - zgl.getCzasOdniesienia());
			System.out.println(simTime() + ": SMO2-Began processing " + zgl);
			// Zaplanuj koniec obsługi
			smoParent.endServiceEvent = new EndServiceBisEvent(smoParent, czasObslugi, zgl);
			// Oznaczenie zdarzenia do opublikowania w obiekcie Dispatcher
			smoParent.endServiceEvent.setPublishable(true);
		}

	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}
}