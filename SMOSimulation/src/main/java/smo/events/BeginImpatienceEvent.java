package smo.events;

import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import smo.model.SmoAnnouncement;

/**
 * 
 * @author Dariusz Pierzchala
 * Description: Zdarzenie początkowe niecierpliwości zgłoszenia. Rozpoczyna niecierpliwość przez losowy czas
 */
public class BeginImpatienceEvent extends BasicSimEvent<SmoAnnouncement, Object>
{
    private SimGenerator generator;
    private SmoAnnouncement parent;

    public BeginImpatienceEvent(SmoAnnouncement parent, double delay) throws SimControlException
    {
    	super(parent, delay);
    	generator = new SimGenerator();
        this.parent = parent;
    }

    public BeginImpatienceEvent(SmoAnnouncement parent) throws SimControlException
    {
    	super(parent);
    	generator = new SimGenerator();
        this.parent = parent;
    }
    
	@Override
	protected void onInterruption() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stateChange() throws SimControlException {
        double odstep = generator.normal(15.0, 1.0);
        System.out.println(simTime()+": Began impatience of: " + parent + " which will last " + odstep + " time units");
        parent.endImpatienceEvent = new EndImpatienceEvent(parent, odstep);
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}
}