package smo.events;
/**
 * @author Dariusz Pierzchala
 * 
 * Description: Zdarzenie końcowe aktywności gniazda obsługi. Kończy obsługę przez losowy czas obiektów - zgłoszeń.
 */

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimEventSemaphore;
import smo.model.Smo;
import smo.model.SmoAnnouncement;

public class EndServiceEvent extends BasicSimEvent<Smo, SmoAnnouncement>
{
    private Smo smoParent;

    public EndServiceEvent(Smo parent, double delay, SmoAnnouncement zgl) throws SimControlException
    {
    	super(parent, delay, zgl);
        this.smoParent = parent;
    }

    public EndServiceEvent(Smo parent, SimEventSemaphore semafor, SmoAnnouncement zgl) throws SimControlException
    {
    	super(parent, semafor, zgl);
        this.smoParent = parent;
    }
    
	@Override
	protected void onInterruption() throws SimControlException {
		// TODO
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stateChange() throws SimControlException {
        if (smoParent.getSmo2().addEntry(transitionParams)) {
    		smoParent.setAvailableForHandling(true);
            System.out.println(simTime()+": Finished processing of entry: " + transitionParams);
            smoParent.monitoredServiceTime.setValue(simTime() - transitionParams.getCzasOdniesienia(), simTime());
        	
            if (smoParent.getSmo2().getAmountOfEntries()==1 && smoParent.getSmo2().isAvailableForServie()) {
            	smoParent.getSmo2().beginServiceEvent = new BeginServiceBisEvent(smoParent.getSmo2());
            }
        	// Zaplanuj dalsza obsługe w tym gnieździe
        	if (smoParent.getAmountOfEntries() > 0)
        	{
        		smoParent.setBeginServiceEvent(new BeginServiceEvent(smoParent));        	
        	}	
        } else {
            System.out.println(simTime()+":  Waiting on semaphore - zgl.: " + transitionParams);
        	smoParent.endServiceEvent = new EndServiceEvent(smoParent, smoParent.getSemafor(), transitionParams);        	
        }
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}
}