package smo.events;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import smo.model.SmoAnnouncement;

/**
 * 
 * @author Dariusz Pierzchala
 * 
 */
public class EndImpatienceEvent extends BasicSimEvent<SmoAnnouncement, Object> {
	private SmoAnnouncement parentEntry;

	public EndImpatienceEvent(SmoAnnouncement parentEntry, double delay) throws SimControlException {
		super(parentEntry, delay);
		this.parentEntry = parentEntry;
	}

	public EndImpatienceEvent(SmoAnnouncement parentEntry) throws SimControlException {
		super(parentEntry);
		this.parentEntry = parentEntry;
	}

	@Override
	protected void onInterruption() throws SimControlException {
		System.out.println(simTime() + ": Interrupted impatience of entry: " + parentEntry);
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void stateChange() throws SimControlException {
		System.out.println(simTime() + ": Finished processing entry: " + parentEntry);
		if (parentEntry.parentSmo.removeEntry(parentEntry)) {
			System.out.println(simTime() + ": Removed the entry from the queue (" + parentEntry + ")");
			double lutrac = parentEntry.parentSmo.monitoredAmountOfLostAnnouncements.getValue();
			parentEntry.parentSmo.monitoredAmountOfLostAnnouncements.setValue(lutrac++);
		} else
			System.out.println(simTime() + ": Couldn't remove entry from the queue (" + parentEntry + ")");
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return null;
	}
}