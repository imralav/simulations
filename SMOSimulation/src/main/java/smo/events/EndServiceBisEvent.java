package smo.events;
/**
 * @author Dariusz Pierzchala
 * 
 * Description: Zdarzenie końcowe aktywności gniazda obsługi. Kończy obsługę przez losowy czas obiektów - zgłoszeń.
 */

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import smo.model.SmoAnnouncement;
import smo.model.SmoBis;

public class EndServiceBisEvent extends BasicSimEvent<SmoBis, SmoAnnouncement>
{
    private SmoBis smoParent;

    public EndServiceBisEvent(SmoBis parent, double delay, SmoAnnouncement zgl) throws SimControlException
    {
    	super(parent, delay, zgl);
        this.smoParent = parent;
    }

	@Override
	protected void onInterruption() throws SimControlException {
        System.out.println(simTime()+": !Interrupted service of: " + transitionParams);			
	}

	@Override
	protected void onTermination() throws SimControlException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stateChange() throws SimControlException {
		// Odblokuj gniazdo
		smoParent.setAvailableForService(true);
        System.out.println(simTime()+": SMO2-Ended service of: " + transitionParams);
		// Zaplanuj dalsza obsługe
        if (smoParent.getAmountOfEntries() > 0)
        {
        	smoParent.beginServiceEvent = new BeginServiceBisEvent(smoParent);        	
        }		
	}

	@Override
	public Object getEventParams() {
		// TODO Auto-generated method stub
		return (SmoAnnouncement) transitionParams;
	}
}