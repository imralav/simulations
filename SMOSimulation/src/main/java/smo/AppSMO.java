package smo;

/**
 * @author Dariusz Pierzchala
 * 
 * Description: Klasa gÅ‚Ã³wna. Tworzy dwa SMO, inicjalizuje je.Startuje symulacjÄ™. WyÅ›wietla statystyki.
 * 
 * Wersja testowa.
 */

import java.math.BigDecimal;
import java.util.Date;

import dissimlab.monitors.Diagram;
import dissimlab.monitors.Diagram.DiagramType;
import dissimlab.monitors.Statistics;
import dissimlab.simcore.SimControlEvent;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimEventSemaphore;
import dissimlab.simcore.SimManager;
import dissimlab.simcore.SimParameters.SimControlStatus;
import smo.model.Smo;
import smo.model.SmoAnnouncementGenerator;
import smo.model.SmoBis;

//class QueueResource extends BasicResourceObj {
//    private Integer position;
//    
//}
//
//class BasicResourceEvent extends BasicSimEvent {
//    
//}
//
//class BasicResourceWarehouse { //czy są jakieś dostawy nieodnawialnych?
//    Queue<Resource> 
//      strategiaDostawy: StrategiaDostawy (pozwoli specyfikować czy dostawa w jakimś konkretnym momencie czy losowo itp)
//                                         (musi dodawać hooki w różne miejsca magazynu, by zliczać kiedy dostawa)
//}
//
///**
// * Klient requestując zasób rejestruje się w magazynie jako ResourceSubscriber
// * Magazyn potem wywołuje resourceReady jeżeli zasób się zwalnia
// * @author karczewt
// *
// */
//interface ResourceSubscriber {
//    resourceReady()
//}

public class AppSMO {
	public static void main(String[] args) {
		try {
//		    ResourceWarehouse warehouse = new ResourceWarehouse();
//		    warehouse.register(QueueResource.class, false, 10);//jaki resource, czy odnawialny, ile jest w magazynie na starcie
//		    
//		    
//		    
//		    warehouse.retrieveResource(this,QueueResource.class,15);//kto pobiera, jaki resource, ile czekania
		    //znaczenie this: kto pobiera resource, by móc potem tworzyć eventy i ewentualnie dodawać resource
		    //ile czekania: 0 - nie czeka, >0 czeka, null - nieskończoność (aż do odnowienia)
		    //jak działa odnowienie? wywłaszczenie, czy zwrócenie po skończeniu korzystania?
		    //odnawialne wracają po tym jak klient już je zwróci
		    
			System.out.println("H - M - S - MS");
			SimManager model = SimManager.getInstance();
			// Utylizator zgÅ‚oszeÅ„
			new FinalDisposer();
			// Semafor
			SimEventSemaphore semafor = new SimEventSemaphore("Semafor dla SMO");
			// PowoÅ‚anie 2-ego Smo
			SmoBis smoBis = new SmoBis(5, semafor);
			// PowoÅ‚anie Smo nr 1
			Smo smo = new Smo(smoBis, semafor);
			// Utworzenie otoczenia
			new SmoAnnouncementGenerator(smo);
			// Dwa sposoby zaplanowanego koÅ„ca symulacji
			// model.setEndSimTime(10000);
			// lub
			new SimControlEvent(1000.0, SimControlStatus.STOPSIMULATION);
			// Badanie czasu trwania eksperymentu - poczÄ…tek
			long czst = new Date().getTime();
			// Uruchomienie symulacji za poÅ›rednictwem metody "start" z
			model.startSimulation();
			// Badanie czasu trwania eksperymentu - koniec
			czst = new Date().getTime() - czst;
			// Wyniki
			System.out.println("Czas trwania eksperymentu: " + czst);

			// prepareAndDrawDiagrams(smoBis, smo);
		} catch (SimControlException e) {
			e.printStackTrace();
		}
	}

	public static void prepareAndDrawDiagrams(SmoBis smoBis, Smo smo) {
		double wynik = new BigDecimal(Statistics.arithmeticMean(smo.monitoredWaitingTime))
				.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		System.out.println("WartoÅ›Ä‡ Å›rednia czasu oczekiwania na obsÅ‚ugÄ™:   " + wynik);
		wynik = new BigDecimal(Statistics.standardDeviation(smo.monitoredWaitingTime))
				.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		System.out.println("Odchylenie standardowe dla czasu obsÅ‚ugi:       " + wynik);
		wynik = new BigDecimal(Statistics.max(smo.monitoredWaitingTime)).setScale(2, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
		System.out.println("WartoÅ›Ä‡ maksymalna czasu oczekiwania na obsÅ‚ugÄ™: " + wynik);
		wynik = new BigDecimal(Statistics.arithmeticMean(smo.monitoredQueueSize)).setScale(2, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
		System.out.println("WartoÅ›Ä‡ Å›rednia dÅ‚ugoÅ›ci kolejki:       " + wynik);
		wynik = new BigDecimal(Statistics.max(smo.monitoredQueueSize)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		System.out.println("WartoÅ›Ä‡ maksymalna dÅ‚ugoÅ›ci kolejki:       " + wynik);

		Diagram d1 = new Diagram(DiagramType.DISTRIBUTION, "Czas obsÅ‚ugiwania");
		d1.add(smo.monitoredServiceTime, java.awt.Color.GREEN);
		d1.show();

		Diagram d2 = new Diagram(DiagramType.DISTRIBUTION, "DlugoÅ›Ä‡ kolejki w SMO nr 2");
		d2.add(smoBis.monitoredQueueSize, java.awt.Color.BLUE);
		d2.show();

		Diagram d3 = new Diagram(DiagramType.HISTOGRAM, "Czasy oczekiwania na obsÅ‚ugÄ™");
		d3.add(smo.monitoredWaitingTime, java.awt.Color.BLUE);
		d3.show();

		Diagram d4 = new Diagram(DiagramType.DISTRIBUTION, "DÅ‚ugoÅ›Ä‡ kolejki w czasie");
		d4.add(smo.monitoredQueueSize, java.awt.Color.RED);
		d4.show();
	}
}
