package smo;
/**
 * @author Dariusz Pierzchala
 * 
 * Description: Obiekt przejmujący zgłoszenia do utylizacji. Działa w wyniku powiadomień z brokera (na sybskrybowane zdarzenia)
 */

import dissimlab.broker.Dispatcher;
import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;
import smo.events.EndServiceBisEvent;
import smo.model.SmoAnnouncement;

public class FinalDisposer extends BasicSimObj {
	//
	public Dispatcher infoDystr;
	//

	public FinalDisposer() throws SimControlException {
		super();
		infoDystr = getDispatcher();
		infoDystr.subscribe(this, EndServiceBisEvent.class);
	}

	@Override
	public void reflect(IPublisher publisher, INotificationEvent event) {
		System.out.println("-Final Disposer: reflect 1 - announcement: "
				+ ((SmoAnnouncement) ((EndServiceBisEvent) event).getEventParams()));
	}

	@Override
	public boolean filter(IPublisher publisher, INotificationEvent event) {
		// Simply true but should be more complex
		if (((SmoAnnouncement) ((EndServiceBisEvent) event).getEventParams()).getTenNr() < 5) {
			System.out.println("-Final Disposer: Filtr OK");
			return true;
		} else {
			// infoDystr.unsubscribe(this, ZakonczObslugeBis.class); //Error due
			// to concurrent modification on Map
			System.out.println("-Final Disposer: Filtr false -> unsubscribe");
			return false;
		}
	}
}