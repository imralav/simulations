package pl.edu.wat.simulations.queues;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FifoQueue<T> extends AbstractQueue<T> {
    protected List<T> elements;

    public FifoQueue() {
        elements = new LinkedList<>();
    }

    public FifoQueue(int limit) {
        super(limit);
        elements = new ArrayList<>(limit);
    }

    @Override
    public T popElement() {
        T poppedElement = elements.remove(0);
        return poppedElement;
    }

    @Override
    public T peekElement() {
        return elements.get(0);
    }

    @Override
    public boolean popElement(T elementToPop) {
        return elements.remove(elementToPop);
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    protected void addElementToQueue(T newElement) {
        elements.add(newElement);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [elements=" + elements + "]";
    }

    @Override
    public List<T> getAllElements() {
        return elements;
    }

    @Override
    public void setElements(List<T> elements) {
        this.elements = elements;
    }
}
