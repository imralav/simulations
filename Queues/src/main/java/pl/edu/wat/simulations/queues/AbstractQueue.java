package pl.edu.wat.simulations.queues;

import java.util.Arrays;

public abstract class AbstractQueue<T> implements Queue<T> {
	int limit = -1;
	
	public AbstractQueue() {
	}
	
	public AbstractQueue(int limit) {
		this.limit = limit;
	}
	
	@Override
	public boolean isLimited() {
		return limit != -1;
	}
	
	@Override
	public final void addElement(T newElement) {
		if(isLimited() && size() >= limit) {
			throw new UnsupportedOperationException("Can't add element - size limit exceeded (" + limit + ")");
		}
		addElementToQueue(newElement);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void addElements(T... elements) {
		Arrays.stream(elements).forEach(element -> {
			addElement(element);
		});
	}

	protected abstract void addElementToQueue(T newElement);
	
	@Override
	public int getLimit() {
		return limit;
	}
}
