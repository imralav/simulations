package pl.edu.wat.simulations.queues.manager;

public interface QueueConverter {
	public void toFifoQueue();
	public void toFiloQueue();
	public void toPriorityQueue(boolean isDescending);
}
