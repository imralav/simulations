package pl.edu.wat.simulations.queues;

import java.util.List;

/**
 * Common interface to the new Queue collection class hierarchy. The collections might be limited, meaning that they
 * won't take more elements than their limit parameter indicates.
 * 
 * @author karczewskit
 *
 * @param <T>
 *            type of the objects that the collection will store
 */
public interface Queue<T> {
    public T peekElement();

    public T popElement();

    public boolean popElement(T elementToPop);

    public void addElement(T newElement);

    @SuppressWarnings("unchecked")
    public void addElements(T... elements);

    public int size();

    public boolean isLimited();

    public int getLimit();

    public List<T> getAllElements();

    public void setElements(List<T> elements);
}
