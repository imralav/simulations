package pl.edu.wat.simulations.queues.manager;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import pl.edu.wat.simulations.queues.FifoQueue;
import pl.edu.wat.simulations.queues.FiloQueue;
import pl.edu.wat.simulations.queues.PriorityQueue;
import pl.edu.wat.simulations.queues.Queue;

/**
 * Wrapper around a regular queue allowing to change the queues type in the runtime
 * 
 * @author karczewskit
 *
 * @param <T>
 */
public class QueueWrapper<T extends Comparable<T>> implements Queue<T>, QueueConverter {
    private Optional<Queue<T>> internalQueue;

    public QueueWrapper(Queue<T> queue) {
        this.internalQueue = Optional.of(queue);
    }

    @Override
    public T popElement() {
        return internalQueue.get().popElement();
    }

    @Override
    public boolean popElement(T elementToPop) {
        return internalQueue.get().popElement(elementToPop);
    }

    @Override
    public T peekElement() {
        return internalQueue.get().peekElement();
    }

    @Override
    public void addElement(T newElement) {
        internalQueue.get().addElement(newElement);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void addElements(T... elements) {
        internalQueue.get().addElements(elements);
    }

    @Override
    public int size() {
        return internalQueue.get().size();
    }

    @Override
    public boolean isLimited() {
        return internalQueue.get().isLimited();
    }

    @Override
    public int getLimit() {
        return internalQueue.get().getLimit();
    }

    @Override
    public void toFifoQueue() {
        Queue<T> queueToConvert = internalQueue.orElseThrow(() -> {
            return new IllegalStateException("No internal queue in the wrapper");
        });
        if (queueToConvert instanceof FifoQueue) {
            return;
        }
        Queue<T> newQueue = null;
        if (queueToConvert.isLimited()) {
            newQueue = new FifoQueue<T>(queueToConvert.getLimit());
        } else {
            newQueue = new FifoQueue<T>();
        }
        newQueue.setElements(queueToConvert.getAllElements());
        this.internalQueue = Optional.of(newQueue);
    }

    @Override
    public void toFiloQueue() {
        Queue<T> queueToConvert = internalQueue.orElseThrow(() -> {
            return new IllegalStateException("No internal queue in the wrapper");
        });
        if (queueToConvert instanceof FiloQueue) {
            return;
        }
        Queue<T> newQueue = null;
        if (queueToConvert.isLimited()) {
            newQueue = new FiloQueue<T>(queueToConvert.getLimit());
        } else {
            newQueue = new FiloQueue<T>();
        }
        newQueue.setElements(queueToConvert.getAllElements());
        this.internalQueue = Optional.of(newQueue);
    }

    @Override
    public void toPriorityQueue(boolean isDescending) {
        Queue<T> queueToConvert = internalQueue.orElseThrow(() -> {
            return new IllegalStateException("No internal queue in the wrapper");
        });
        if (queueToConvert instanceof PriorityQueue) {
            return;
        }
        Queue<T> newQueue = null;
        if (queueToConvert.isLimited()) {
            newQueue = new PriorityQueue<T>(queueToConvert.getLimit(), isDescending);
        } else {
            newQueue = new PriorityQueue<T>();
        }
        List<T> oldElements = queueToConvert.getAllElements();
        Collections.sort(oldElements, new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (isDescending) {
                    return o2.compareTo(o1);
                } else {
                    return o1.compareTo(o2);
                }
            }
        });
        newQueue.setElements(oldElements);
        this.internalQueue = Optional.of(newQueue);
    }

    @Override
    public List<T> getAllElements() {
        return internalQueue.get().getAllElements();
    }

    @Override
    public void setElements(List<T> elements) {
        internalQueue.get().setElements(elements);
    }

    @Override
    public String toString() {
        return "QueueWrapper [internalQueue=" + internalQueue + "]";
    }
}
