package pl.edu.wat.simulations.queues;

public class FiloQueue<T> extends FifoQueue<T> {
	public FiloQueue() {
		super();
	}
	
	public FiloQueue(int limit) {
		super(limit);
	}
	
	@Override
	protected void addElementToQueue(T newElement) {
		elements.add(0,newElement);
	}
}
