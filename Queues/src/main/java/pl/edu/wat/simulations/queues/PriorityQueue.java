package pl.edu.wat.simulations.queues;

public class PriorityQueue<T extends Comparable<T>> extends FifoQueue<T> {
	private boolean descending = true;
	
	public PriorityQueue() {
		super();
	}
	
	public PriorityQueue(boolean descending) {
		super();
		this.descending = descending;
	}
	
	public PriorityQueue(int limit, boolean descending) {
		super(limit);
		this.descending = descending;
	}
	
	@Override
	protected void addElementToQueue(T newElement) {
		for(int i = 0; i<size(); i++) {
			T currentElement = elements.get(i);
			if((descending && currentElement.compareTo(newElement) < 0) || (!descending && currentElement.compareTo(newElement) > 0)) {
				elements.add(i, newElement);
				return;
			}
		}
		elements.add(newElement);
	}
	public boolean isDescending() {
		return descending;
	}
}
