package pl.edu.wat.simulations;

import pl.edu.wat.simulations.queues.FifoQueue;
import pl.edu.wat.simulations.queues.Queue;
import pl.edu.wat.simulations.queues.manager.QueueWrapper;

public class App {
	public static void main(String[] args) {
		Queue<Integer> queue = new FifoQueue<Integer>();
		QueueWrapper<Integer> wrapper = new QueueWrapper<>(queue);
		queue.addElements(2,1,5,2,6,4);
		System.out.println(wrapper);
		wrapper.toPriorityQueue(true);
		System.out.println(wrapper);
	}
}
