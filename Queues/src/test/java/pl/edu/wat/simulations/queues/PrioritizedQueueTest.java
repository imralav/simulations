package pl.edu.wat.simulations.queues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.Test;

public class PrioritizedQueueTest {
	private Queue<Integer> sut;
	@Test
	public void shouldPrioritizeBiggerIntegersForDescendingOrder() {
		// given
		sut = new PriorityQueue<>();
		// when
		sut.addElements(2,1,5,6,5);
		Integer popElement = sut.popElement();
		// then
		assertThat(popElement).isEqualTo(6);
	}
	
	@Test
	public void shouldPrioritizeLowerIntegerForAscendingOrder() {
		//given
		sut = new PriorityQueue<>(false);
		// when
		sut.addElements(2,1,5,6,5);
		Integer popElement = sut.popElement();
		// then
		assertThat(popElement).isEqualTo(1);
	}
	
	@Test
	public void shouldLimitElements() {
		//given
		sut = new PriorityQueue<>(2,true);
		//when
		//then
		assertThatThrownBy(() -> { sut.addElements(1,1,1); }).isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining("Can't add element - size limit exceeded")
        .hasMessageEndingWith("(" + sut.getLimit() + ")");
	}
}
