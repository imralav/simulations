package pl.edu.wat.simulations.queues.manager;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import pl.edu.wat.simulations.queues.FifoQueue;
import pl.edu.wat.simulations.queues.Queue;

public class QueueWrapperTest {
	QueueWrapper<Integer> sut;

	@Test
	public void shouldChangeFifoToFilo() {
		// given
		Queue<Integer> queue = new FifoQueue<>();
		sut = new QueueWrapper<>(queue);
		sut.addElements(3, 2, 1);
		// when
		sut.toFiloQueue();
		// then
		sut.addElement(5);
		Integer popElement = sut.popElement();
		assertThat(popElement).isEqualByComparingTo(5);
	}

	@Test
	public void shouldChangeToPriorityWithDescendingOrder() {
		// given
		Queue<Integer> queue = new FifoQueue<>();
		sut = new QueueWrapper<>(queue);
		sut.addElements(1, 5, 2, 3, 4, 6, 4);
		// when
		sut.toPriorityQueue(true);
		// then
		Integer popElement = sut.popElement();
		assertThat(popElement).isEqualByComparingTo(6);
	}

	@Test
	public void shouldChangeToPriorityWithAscendingOrder() {
		// given
		Queue<Integer> queue = new FifoQueue<>();
		sut = new QueueWrapper<>(queue);
		sut.addElements(2, 1, 5, 2, 3, 4, 6);
		// when
		sut.toPriorityQueue(false);
		// then
		Integer popElement = sut.popElement();
		assertThat(popElement).isEqualByComparingTo(1);
	}
}
