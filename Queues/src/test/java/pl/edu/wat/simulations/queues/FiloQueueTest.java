package pl.edu.wat.simulations.queues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.Test;

public class FiloQueueTest {
	Queue<String> sut;
	@Test
	public void shouldPopLastAddedElement() {
		// given
		sut = new FiloQueue<>();
		// when
		sut.addElements("a","b");
		sut.addElement("c");
		String popElement = sut.popElement();
		// then
		assertThat(popElement).isEqualTo("c");
	}
	
	@Test
	public void shouldLimitElementAmount() {
		//given
		sut = new FiloQueue<>(2);
		//when
		//then
		assertThatThrownBy(() -> {
			sut.addElements("a","b","c");
		}).isInstanceOf(UnsupportedOperationException.class)
		.hasMessageContaining("Can't add element - size limit exceeded")
        .hasMessageEndingWith("(" + sut.getLimit() + ")");
	}
}
