package pl.edu.wat.simulations.queues;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class FifoQueueTest {
	Queue<String> sut;
	@Test
	public void shouldPopFirstAddedItem() {
		// given
		sut = new FifoQueue<>();
		// when
		sut.addElements("a","b");
		sut.addElement("c");
		String popElement = sut.popElement();
		// then
		assertThat(popElement).isEqualTo("a");
	}
}
