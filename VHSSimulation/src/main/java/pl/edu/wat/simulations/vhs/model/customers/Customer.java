package pl.edu.wat.simulations.vhs.model.customers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.random.SimGenerator;
import dissimlab.resourceManagement.warehouse.BasicResourceObject;
import dissimlab.resourceManagement.warehouse.BasicWarehouse;
import dissimlab.resourceManagement.warehouse.ResourceListener;
import dissimlab.resourceManagement.warehouse.exceptions.UnregisteredResourceRequested;
import dissimlab.simcore.BasicSimObj;

public class Customer extends BasicSimObj implements ResourceListener{
    private Logger logger = LoggerFactory.getLogger(getClass());
	private static int customerAmount = 0;
	private int id;
	
	public Customer() {
		id = ++customerAmount;
	}

    @Override
	public String toString() {
		return "Customer [id=" + id + "]";
	}

	@Override
    public void reflect(IPublisher publisher, INotificationEvent event) {
        
    }

    @Override
    public boolean filter(IPublisher publisher, INotificationEvent event) {
        return false;
    }

	@Override
	public void resourceReady() {
		
	}

    @Override
    public void resourceReturned(Class<? extends BasicResourceObject> returnedResourceClass, BasicWarehouse warehouse) {
        logger.info("Resource {} returned for customer {} from warehouse {}",returnedResourceClass, this, warehouse);
        logger.info("Ordering the resource again");
        SimGenerator generator = new SimGenerator();
        try {
            warehouse.orderRequest(this, returnedResourceClass, generator.nextInt(15), generator.nextInt(15));
        } catch (UnregisteredResourceRequested e) {
            //shouldn't happen
            logger.error(e.getLocalizedMessage(), e);
        }
    }
}
