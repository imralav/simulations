package pl.edu.wat.simulations.vhs;

import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dissimlab.resourceManagement.warehouse.BasicWarehouse;
import dissimlab.resourceManagement.warehouse.renewal.ResourceAmountRenewalStrategy;
import dissimlab.resourceManagement.warehouse.renewal.ResourceRenewalStrategy;
import dissimlab.simcore.SimControlEvent;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimManager;
import dissimlab.simcore.SimParameters.SimControlStatus;
import pl.edu.wat.simulations.vhs.model.customers.VHSCustomersGenerator;
import pl.edu.wat.simulations.vhs.model.resources.CDRecord;
import pl.edu.wat.simulations.vhs.model.resources.VHSRecord;

public class Simulation {
    private static final Logger logger = LoggerFactory.getLogger(Simulation.class);
    private static final int INITIAL_VHS_AMOUNT = 1;
    private SimManager model;
    private double simulationTime;

    public Simulation(double simulationTime) {
        this.simulationTime = simulationTime;
    }

    public static void main(String[] args) throws SimControlException, InstantiationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        double simulationTime = 500.0;
        Simulation simulation = new Simulation(simulationTime);
        simulation.prepareData();
        logger.info("Starting simulation...");
        simulation.start();
        logger.info("The simulation has ended");
    }

    private void start() throws SimControlException {
        model.startSimulation();
    }

    private void prepareData() throws SimControlException {
        model = SimManager.getInstance();
        scheduleSimulationEndEvent();
        BasicWarehouse warehouse = new BasicWarehouse(prepareRenewalStrategy());
        registerResources(warehouse);
        new VHSCustomersGenerator(warehouse);
    }

    private ResourceRenewalStrategy prepareRenewalStrategy() {
        return new ResourceAmountRenewalStrategy((actualAmount, maxAmount) -> {
            return actualAmount < 0.5 * maxAmount;
        });
    }

    private void registerResources(BasicWarehouse warehouse) {
        try {
            warehouse.registerResource(VHSRecord.class, true, INITIAL_VHS_AMOUNT);
            warehouse.registerResource(CDRecord.class, true, INITIAL_VHS_AMOUNT);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException e) {
            logger.error("Error when registering resource", e);
        }
    }

    private void scheduleSimulationEndEvent() throws SimControlException {
        new SimControlEvent(simulationTime, SimControlStatus.STOPSIMULATION);
    }
}
