package pl.edu.wat.simulations.vhs.model.customers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dissimlab.random.SimGenerator;
import dissimlab.resourceManagement.warehouse.exceptions.UnregisteredResourceRequested;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import pl.edu.wat.simulations.vhs.model.resources.VHSRecord;

public class CustomerEntersEvent extends BasicSimEvent<VHSCustomersGenerator, Object> {
	private static final int MAX_WAIT_TIME = 16;
    private static final Logger logger = LoggerFactory.getLogger(CustomerEntersEvent.class);
    private static final int MAX_BORROW_TIME = 15;
	private SimGenerator generator;

    public CustomerEntersEvent(VHSCustomersGenerator parent, double delay) throws SimControlException{
    	super(parent,delay);
    	generator = new SimGenerator();
	}

	@Override
    public Object getEventParams() {
        return null;
    }

    @Override
    protected void stateChange() throws SimControlException {
        Customer customer = new Customer();
        logger.info("{} New customer ({}) enters the VHSWarehouse and orders VHS", simTime(), customer);
    	int waitTime = generator.nextInt(MAX_WAIT_TIME);
    	int borrowTime = generator.nextInt(MAX_BORROW_TIME);
    	VHSRecord requestedRecord = requestRecord(customer, waitTime, borrowTime);
    	if(requestedRecord == null) {
    	    logger.info("{} No record returned (it is not available, waiting for renewal for {} time units", simTime(), waitTime);
    	} else {
    	    logger.info("{} Record ordered ({})", simTime(), requestedRecord, borrowTime);
    	    if(requestedRecord.isReturnable()) {
    	        logger.info("{} Starting return event (with return time of {} time units)", simTime(), borrowTime);
    	    }
    	}
        setRepetitionPeriod(generator.normal(10, 5));
    }

    private VHSRecord requestRecord(Customer customer, int waitTime, int borrowTime) {
        try {
            return getSimObj().getWarehouse().orderRequest(customer, VHSRecord.class, waitTime, borrowTime);
        } catch (UnregisteredResourceRequested e) {
            logger.error("{} {} requested unregistered resource (root cause: {})", simTime(), customer, e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onTermination() throws SimControlException {
        
    }

    @Override
    protected void onInterruption() throws SimControlException {
        
    }

}
