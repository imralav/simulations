package pl.edu.wat.simulations.vhs.model.resources;

import dissimlab.resourceManagement.warehouse.BasicResourceObject;

public class VHSRecord extends BasicResourceObject{
	public VHSRecord(int id, boolean isRenewable) {
		super(id, isRenewable);
	}
}
