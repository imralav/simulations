package pl.edu.wat.simulations.vhs.model.customers;

import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.resourceManagement.warehouse.BasicResourceObject;
import dissimlab.resourceManagement.warehouse.BasicWarehouse;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;

public class VHSCustomersGenerator extends BasicSimObj {
	private BasicWarehouse warehouse;

	public VHSCustomersGenerator(BasicWarehouse warehouse) throws SimControlException {
    	this.warehouse = warehouse;
        new CustomerEntersEvent(this, 0.0);
    }

    @Override
    public void reflect(IPublisher publisher, INotificationEvent event) {
        
    }

    @Override
    public boolean filter(IPublisher publisher, INotificationEvent event) {
        return false;
    }
    
    public BasicWarehouse getWarehouse() {
		return warehouse;
	}

    @Override
    public void resourceReturned(Class<? extends BasicResourceObject> returnedResourceClass,
            BasicWarehouse warehouse) {
    }
}
